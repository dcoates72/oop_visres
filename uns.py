#from np import * # unique, etc
import numpy as np

def conf_from_sims(sim):
    simsize = np.shape(sim)[0]
    sortd = np.sort( sim )
    meanval1 = (sortd[simsize/2] + sortd[simsize/2-1])/2.0
    meanval2 = np.mean( sim )
    anstd = np.std( sim )
    m95 = sortd[simsize*0.025]
    p95 = sortd[-simsize*0.025]
    return meanval1, m95,p95

def arrr(arr, fmt='%0.2g', pmsign=True, ranges=True, ci=False):
    arr= np.array(arr)
    if len(np.shape(arr))>1:
        arr = arr.flatten()
    confI = conf_from_sims(arr) 
    if pmsign:
        # TOO HARD, coulnd't figure out how to do unicode easily from http://docs.python.org/2/howto/unicode.html
        fmtstr = '%s+/-%s min=%s max=%s %s 95%%%%:%s-%s' % (fmt, fmt, fmt, fmt, fmt, fmt, fmt)
    else:
        fmtstr = '%s+/-%s min=%s max=%s %s 95%%%%:%s-%s' % (fmt, fmt, fmt, fmt, fmt, fmt, fmt)
    #st = '%0.2g+-%0.2g min=%0.2g max=%0.2g %0.2g %0.2g-%0.2g' % (arr.mean(), arr.std(), arr.min(), arr.max(), confI[0], confI[1], confI[2] )
    st = fmtstr % (arr.mean(), arr.std(), arr.min(), arr.max(), confI[0], confI[1], confI[2] )

    if ranges==False:
        st = st[0:st.find(' ')]

    return st

def unhist(d,vals=[], scalegrp=[] ):
	global scalers, scalelist, idxs, val3, scalecounts
	vals2,idxs = unidx(d,vals)
	counts = [len(idx) for idx in idxs]

	#try:
	if len(scalegrp) > 0:
	#if not isscalar( scalegrp):
		vals3,scalecounts= unhist(scalegrp,vals) # NOT RECURSIVE!
		#vals3,scalers = [len(scalelist) for idx in idxs]
		#print d
		#print idxs
		#print scalecounts
		#print counts
		counts = np.array(counts, dtype='float') / scalecounts
	#except:
		#scalers=1

	return vals2, counts

def unidx(d, invals=[] ):
	global dbgflag
	if len(invals) == 0:
		invals = np.unique( d ) # default: just use unique values
		vals = [[int(val)] for val in invals]

	try:
		if (len(invals)  == np.size(invals)) : # if is one-dimensional
			vals = [[val] for val in invals]
		else:
			vals = invals
	except:
			vals = invals

	idxs =  [ np.concatenate( ([np.where( d == (oneval))[0] for oneval in val]) ) for val in vals ]

	try:
		# TODO: Need to fix flatten
		if len(np.shape(vals)) == 1:
			vals = np.array(vals).flatten()
		elif (len(np.shape(vals))) > 1:
			if np.shape(vals)[1] == 1:
				vals = np.array(vals).flatten()
	except ValueError:
		x=0 #print "." #Weird: can't flatten vals in unidx()"

	return vals, idxs

def sortord(data):
	keyeddata = np.array( [ (i,n) for n,i in enumerate(data)], [('value', float),    ('key', int)] )
	sorteddata = np.sort( keyeddata, order='value')
	return sorteddata['value'], sorteddata['key']
 
orda = ord('a')
def idxtolets(idx):
	div = idx/26
	rem = idx - div*26
	return chr(div+orda) + chr(rem+orda)

def pylinefit(y,x=None):
	global A
	if x==None:
		x=np.arange(len(y))
	# Fit a line, ``y = mx + b``, through some noisy data-points:
	A = np.vstack([x, np.ones(len(x))]).T
	m, b = np.linalg.lstsq(A, y)[0]
	return m,b

