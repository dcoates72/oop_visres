from psychopy import visual,core

# Multiple inheritance
class random_letter_raw(object):
    def generate(self):
        self.letter=chr(ord('a')+np.random.randint(26))
        
class random_letter_number(object):
    def generate(self):
        self.letter=chr(ord('0')+np.random.randint(10))

class wrap_psychopy(object):
    def __init__(self, win, pos):
        self._widget=visual.TextStim(win, pos=pos)
        print 'created widget'
    def draw(self):
        self._widget.text=self.letter
        self._widget.draw()
        
class wrap_matplotlib(object):
    def __init__(self, win, pos):
        self.pos=pos
        print 'saved position'
    def draw(self):
        plt.text( self.pos[0], self.pos[1], self.letter )

class letter_matplotlib(random_letter_raw,wrap_matplotlib):
    pass

class letter_psychopy(random_letter_raw,wrap_psychopy):
    pass


win = visual.Window(size=(600,400), color='gray', units='pix')
a=letter_psychopy( win, (0,0) )
b=letter_psychopy( win, (30,0) )
a.letter="h"
b.letter="i"
a.draw()
b.draw()
win.flip()
core.wait(3)
win.close()
