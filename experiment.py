import psychopy.visual as visual
import psychopy.event as event
import psychopy.core as core
from psychopy import log
import psychopy.data as pd 
import numpy as np
import time
import matplotlib.pyplot as pyplot

import util
import stims
import importlib
import datetime
import conditions

def generate_seq(params):
	if params.target_sequence=="balanced":
		if params.letter_set=="Sloan":
			outof=10
		elif params.letter_set=="S5":
			outof=5
		elif params.letter_set=="custom":
			outof=len(params.letter_set_custom)
		else:
			raise ValueError("Can't handle this font type.")
	
		validpairs = [ (x*10+y) for x in xrange(outof) for y in xrange(outof) if (x!=y)]
		#x=np.tile( np.arange(100), ((params.ntrials-1)/90+1) ) # permute numbers from 1-100
		reps = np.tile( validpairs, np.ceil( float(params.ntrials)/len(validpairs) ) )
		params.target_seq = np.random.permutation(reps)[0:params.ntrials]

		params.flankerseq = np.concatenate( np.tile( params.spacings_cs, (18,1)).T )
		if params.spacings_blocked==False:
			params.flankerseq=np.random.permutation( params.flankerseq )


# see: for all the myWin.blendMode='add' peppered in to solve some bug.
#http://stackoverflow.com/questions/32184164/psychopy-bad-contrast-in-blendmode-add
class Experiment:
	def __init__(self, param_filename, docalib=True):
		self.params=importlib.import_module(param_filename)
		self.docalib=docalib

	def makeStims(self, myWin, params):
		rings = stims.FlankedRings( "rings", myWin, params)
		stims_all = [rings]
		return stims_all,[rings]

	def run(self):
		params=self.params
		conditions.translate_conditions(params) # remap based on condition code shortcuts
		generate_seq( params )
		# Set up the screen, output file, etc.
		myWin = visual.Window(params.screendim, allowGUI=True, rgb=(0,0,0), units='pix',
					fullscr=params.fullscr, winType='pyglet', blendMode='add', useFBO=True )
		myWin.setMouseVisible(False)
		myWin.setRecordFrameIntervals(True)
		log.console.setLevel(log.ERROR)

		outfilename = util.get_unique_filename("results/%s_%s_%s-%%02d.csv" % (params.SubjectName,
				params.condition,time.strftime("%m%d%Y", time.localtime() ) ) )

		outfile = open(outfilename, "wt")
		util.dumpvars(params,outfile) 

		# Fixation (&helper?):
		# Need to create this first in order to do calibration
		fixation = visual.TextStim(myWin,pos=params.fixation_pos,alignHoriz='center', alignVert='center', height=9, color=(1.0,1.0,1.0), ori=0) 
		fixation.setText( params.fixation_char )

		if self.docalib:
			fliprate=util.calibrate_timing( myWin, fixation, event) 
		else:
			fliprate=0.03 # filler
		print 'fliprate=%f' % fliprate

		xval = 0
		done = False
		trialNum=0

		fixation.setHeight( params.fixation_size )
		fixation.setText( params.fixation_char )
		fixation.draw()
		myWin.flip()
		event.waitKeys()

		# Trial initialization:
		x_correct = [1.0]
		x_incorrect = [0.01]
		duration=params.trial_time
		if params.method=="cs" or params.method=="hybrid":
			trials=np.random.permutation( np.tile( params.cs_steps, params.ntrials/len(params.cs_steps)) )
			if params.method=="cs":
				trials=trials+params.cs_center*100.0 # TODO:100 sketchy for some variable types
				newval=trials[0]
			elif params.method=="hybrid":
				newval=50 # start at halfway
		else: #staircase
			parms=util.mlpf( x_correct, x_incorrect )[0]
			newval=parms[0]*100 # start at mean # TODO 100 not good
		print newval
		flanker_spacing=-1 # so that something is there for the output routine
		if params.variable=="spacing":
			flanker_spacing=newval
			siz=params.size
		elif params.variable=="duration":
			duration=params.trial_time
			siz=params.size
		elif params.variable=="size":
			siz=newval
		else: # not sure what it is
			siz=params.size
		
		# Stimuli:
		stims_all,targets = self.makeStims(myWin, params)
	
		noise_mask = visual.ImageStim( myWin, texRes=1, mask="none",
                        pos=(0,0), units='pix', size=(params.size_noise,params.size_noise), colorSpace='rgb', color=[1,1,1] )
		
		myWin.blendMode='add'
		
		resp_map = {'num_6': 0, 'num_9': 1, 'num_8': 2, 'num_7': 3, 'num_4': 4, 'num_1': 5, 'num_2': 6, 'num_3': 7 }
		
		totalcorrect=0 # just for informational purposes 
		trialNum=0

		# Run loop:
		while not done:
			noise_bits = np.random.rand(params.size_noise,params.size_noise)*2.0-1.0 # gen noise 
			noise_mask.image = noise_bits

			myWin.blendmode='add'
			fixation.draw()
			myWin.flip()
		
			[astim.update(siz,trialNum) for astim in targets]
		
			myWin.blendmode='add'
			if params.foveal_helper:
				targets[0].targets._helper.draw()
			else:
				fixation.draw()
			myWin.flip()
		
			if params.pre_time>0:
				core.wait(params.pre_time)
		
			if params.foveal_helper:
				real_duration = util.draw1(duration, stims_all, myWin, fliprate, ramp=False, mask=False)
			else:
				real_duration = util.draw1(duration, stims_all+[fixation], myWin, fliprate, ramp=False, mask=False)
		
			if params.mask_time > 0:
				myWin.blendMode='avg' # otherwise color is messed up
				fixation.draw()
				noise_mask.draw()
				myWin.flip()
				core.wait(params.mask_time) # timing not perfect, but ok for mask
		
			if ((params.trial_time >= 0) and (params.mode=="erase")):
				myWin.blendmode='add'
				fixation.draw()
				myWin.flip()

			time_on=datetime.datetime.now()

			response=-1
			for key in event.waitKeys():
				if key in [ 'escape' ]:
					#core.quit()
					done = True
				elif key=='i':
					myWin.blendMode='add'
					util.draw1(duration, stims_all+[fixation], myWin, fliprate, ramp=False, mask=False)
					myWin.getMovieFrame()   # Defaults to front buffer, I.e. what's on screen now.
					myWin.saveMovieFrames('screenshot.png')
					key=event.waitKeys()
				else:
					try:
						response=resp_map[key]
					except KeyError:
						pass
		
			# TODO: Need a better (general) method to access target
			target_which=stims_all[0].target_which
			target_let=stims_all[0].target_let
			untarget_let=stims_all[0].untarget_let

			timediff = datetime.datetime.now()-time_on
			outfile.write ('%d,%d,%g,%s,%s,%d,%g,%g\n' % ( response, target_which, siz,
				 target_let, untarget_let, flanker_spacing, real_duration, int(timediff.total_seconds()*1000.0) ) )
		
			iscorrect = (response == target_which)
			totalcorrect += iscorrect
			
			if params.test_flanker_spacing:
				value=flanker_spacing/100.
			elif params.test_duration:
				value=duration
			else:
				value=siz/100.
			if iscorrect:
				x_correct = np.concatenate( (x_correct, [value] ) )
			else:
				x_incorrect = np.concatenate( (x_incorrect, [value] ) )
		
			trialNum += 1
			if trialNum >= params.ntrials:
				done = True
		
                        # Trial handling: determine next level for staircase
			if done==False:
				if params.cs:
					if params.hybrid==False:
						newval=trials[trialNum] # already has center pre-added
					else: # Hybrid
						parms=util.mlpf( x_correct, x_incorrect )[0] # FIT
						if params.test_duration: B=1.0
						else: B=100
						parms_fixed=util.fixparms( parms, B )
						offset=trials[trialNum]
						if B==1:
							newval=parms_fixed[0] + parms_fixed[1]*offset
						elif B==100:
							# Use a minimum of 1 pixel for slope, when modulating size .. (ensure moving by at least one pixel)
							newval=int( parms_fixed[0] + max(1,parms_fixed[1])*offset )
						print parms, parms_fixed, offset, newval
						
				else:
					parms=util.mlpf( x_correct, x_incorrect, inits=parms )[0]
					#print x_correct
					#print x_incorrect
					print parms
					# Go either up or down 1 sigma from estimated mean
					adir=np.random.randint(2)*2-1
					adelta=int( parms[0]/util.fixparms(parms)[1]/10.0 )
					if adelta<1.0: adelta=1 # always test up/down 1 pixel size
					newval=int( 100*parms[0]+adelta*adir )
					print newval,adelta,adir
		
				if params.test_flanker_spacing:
					flanker_spacing=int( newval )
					if flanker_spacing<10: flanker_spacing=10
					if (flanker_spacing>100) and (params.cs_center<1.0): flanker_spacing=100
				elif params.test_duration:
					duration=newval
					print duration
					if duration<0.030: duration=0.03
					if (duration>0.5): duration=0.5
				elif params.test_size:
					siz=newval
					if siz<6: siz=6
					if siz>100: siz=100
		
		# after the runloop exits:
		myWin.close()
		outfile.close()
		print totalcorrect, params.ntrials
