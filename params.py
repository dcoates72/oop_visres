from numpy import pi 

fullscr = False 
#screendim = [1024,768]
screendim = [1280,1024]
screen_height_cm = 30 # in cm
distance_cm=40
fixation_pos=(0,0) 
ntrials = 100
color_def=(1,1,1)
size_noise=1024

# Trial control
mode="erase"
method="hybrid" # "cs|hybrid|staircase"
variable="duration" # size|duration|spacing
#variable_type="absolute" # absolute|nominal
cs_steps=[-2,-1,0,1,2] #[-20,-10,0,10,20]
cs_center=0.0

# Times in seconds:
trial_time =  0.900 
pre_time = 0.1
mask_time = 0.1
between_time = 0.000
noise_on_time=0.100

# Fixation
fixation_char='.'
fixation_size=28

# Ring parameters:
num_spokes=8
radius=300
phase=0/4.0 # or pi/4.0, e.g.
size=30 
char_target_rand=True
char_target='T'
char_untarget='L'
font='Times'
ori=0
flanker_spokes=8
jitter_amt=00.0 # affects all LetterRings

# Flankers
flankers=True
flanker_spacing_nominal=False # If false, pixels, if True, is "X" of size
char_flanker_i='O'
char_flanker_o='O'
char_target='N'
char_untarget='Z'
char_target_rand=True  
target_sequence="balanced"   # "blocked"
orientation = 0 # orientation
letter_set = "Sloan" # or custom
letter_set_custom = ['P', 'R']
flanker_texture=False # Make flanker match untarget
flankers_alternate=False # don't flank everything
flankers_random=True 
targets_alternate=False # don't flank everything

# Helper
foveal_helper=True
helper_orientation = orientation # matches orientation of targets
helper_identity_flanker=False
helper_identity_uninformative=False # uninformative helper
helper_polarity_reverse=False


# To remove:
bar_flankers=False # Bars aren't really tested..

method="hybrid"
do_stair = False
hybrid=True
test_flanker_spacing=False
test_duration=True
cs=True
cs_center=50
cs_steps=[0] #[-2,-1,0,1,2] #[-20,-10,0,10,20]
test_flanker_spacing=False
test_duration=False
test_size=False
hybrid=False
size_fixed=40

lineWidth=8 # for two-line

# --- Params for our first experiment:
# By default, doing a CS at a single setting, unflanked
flanker_spacing=50.0 
r=276 # radius of target or untarget : 276=8deg
c1_dist=45
c2_dist=100
c3_dist=150
cc_dist=60

size_cs_logspace=(10,60,5)

do_spacings=True
spacings_cs = [5000, 80, 100, 120, 150]
spacings_blocked = False

# Change the things below for each subject/condition
# --------------------------------------------
# Conditions:
#   0==calibration for size reading
#   1==calibration for size reading, crowded
#   0H==calibration for size reading, with helper
#   C==Crowded near
#   CC==Crowded medium
#   CCC==Crowded far
#   U=Uncrowded
#   HC=Crowded (2), with helper
#   HCU=Crowded (2), With uninformative helper
#   H=Uncrowded, helper
#   HU=Uncrowded, uninformative helper
#   S=Sizes, constant stimuli

#   16T16=Texture: 16X16, matching
#   16X16=16 spokes, 16 X flankers
#   16X8=16 spokes, 8 X flankers
#   16T8=16 spokes, 8 texture flankers
#   8T16=16 spokes, 8 texture flankers
#   8T8=16 spokes, 8 texture flankers
#   8X8=8 spokes, 8 X flankers
#   16T16C=Texture: 16X16, matching, closer-spacing

SubjectName = 'xx'
#condition="2L-4C"
#condition="16X16"
condition="XXX"
cs_center=50
