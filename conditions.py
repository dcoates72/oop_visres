import numpy as np

def translate_conditions(params):
	if params.condition=="0":
		params.cs_steps=np.array([0,1,2,3,4])
		params.hybrid=True
	elif params.condition=="1":
		params.cs_steps=np.array([0,1,2,3,4])
		params.hybrid=True
		params.flanker_spacing=params.cc_dist
	elif params.condition=="S":
		params.cs_steps=10**np.linspace(np.log10(params.size_cs_logspace[0]), np.log10(params.size_cs_logspace[1]), params.size_cs_logspace[2])
		params.hybrid=False
	elif params.condition=="0H":
		params.foveal_helper=True
		params.cs_steps=np.array([0,1,2,3,4])
		params.hybrid=True
	elif params.condition=="C":
		params.flanker_spacing=params.c1_dist
	elif params.condition=="CC":
		params.flanker_spacing=params.c2_dist
	elif params.condition=="CCC":
		params.flanker_spacing=params.c3_dist
	elif params.condition=="U":
		pass #default
	elif params.condition=="HC":
		params.foveal_helper=True
		params.flanker_spacing=params.c2_dist
	elif params.condition=="HCU":
		params.foveal_helper=True
		params.helper_identity_uninformative=True
		params.flanker_spacing=params.c2_dist
	elif params.condition=="H":
		params.foveal_helper=True
	elif params.condition=="HU":
		params.foveal_helper=True
		params.helper_identity_uninformative=True
	elif params.condition=="8T8":
		params.flanker_spacing=params.cc_dist
		params.flanker_texture=True
	elif params.condition=="8X8":
		params.flanker_spacing=params.cc_dist
	elif params.condition=="16X8":
		params.num_spokes=16
		params.flanker_spokes=8
		#params.flankers_alternate=True
		params.flanker_spacing=params.cc_dist
	elif params.condition=="16X16":
		params.num_spokes=16
		params.flanker_spokes=16
		params.flanker_spacing=params.cc_dist
	elif params.condition=="16T16":
		params.num_spokes=16
		params.flanker_spacing=params.cc_dist
		params.flanker_texture=True
	elif params.condition=="16T16C":
		params.num_spokes=16
		params.flanker_spacing=params.c1_dist
		params.flanker_texture=True
	elif params.condition=="16T8":
		params.num_spokes=16
		params.flanker_texture=True
		params.flankers_alternate=True
		params.flanker_spacing=params.cc_dist
	elif params.condition=="8T16":
		params.num_spokes=8
		params.flanker_spokes=16
		params.flanker_spacing=params.cc_dist
		params.flanker_texture=True
		#params.targets_alternate=True
	elif params.condition=="8X16":
		params.num_spokes=8
		params.flanker_spokes=16
		params.flanker_spacing=params.cc_dist
		params.targets_alternate=False
	elif params.condition=="T":
		params.flanker_texture=True
	elif params.condition=="2L-4C":
		params.num_spokes=16
		params.flanker_spacing=params.cc_dist
	
