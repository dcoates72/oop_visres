all:
	jupyter notebook oop_visres.ipynb

run:
	python -c 'import experiment; experiment.Experiment("params",docalib=False).run()'

gallery:
	python -c 'import gallery; gallery.Experiment("params",docalib=False).run()'
